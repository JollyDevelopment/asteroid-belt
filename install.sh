#!/bin/bash
gunzip Asteroids.tar.gz

tar -xvf Asteroids.tar

path="${HOME}/.local/share"

mkdir -p "$path/asteroid-belt"

if [ $? -ne 0 ]; then 
	echo ""
	echo "Could not create folder '$path/asteroid-belt'"
	exit
fi

echo "" 
echo "Copying download folder to $path/asteroid-belt"
cp -ru ./* "$path/asteroid-belt"

if [ $? -ne 0 ]; then
	echo "" 
	echo "Could not copy download folder to $path/asteroid-belt"
	exit
fi

echo "" 
echo "Create a new desktop file"
rm -f "$path/applications/asteroid-belt.desktop"

echo "[Desktop Entry]
Name=Asteroid Belt
X-GNOME-FullName=Asteroid Belt
Comment=Game built for the PP
Keywords=Game; Godot;
Exec="$path/asteroid-belt/Asteroids_ARM.x86"
Terminal=false
StartupNotify=true
Type=Game
Icon=$path/asteroid-belt/logo.svg
Categories=GNOME;GTK;Game;
Name[en_US]=Asteroid Belt
X-Purism-FormFactor=Workstation;Mobile;" > "$path/applications/asteroid-belt.desktop"

if [ $? -ne 0 ]; then 
	echo "" 
	echo "Could not create desktop file in to $path/applications"
	exit
fi

#rm -f "$path/run_free.sh"

#echo "#!/usr/bin/bash
#cd '$path/asteroid-belt'
#python3 run_free.py" > "$path/run-free/run_free.sh"

chmod 0755 "$path/asteroid-belt/Asteroids_ARM.x86"

if [ $? -ne 0 ]; then 
	echo "" 
	echo "Could not create shell script in to $path/asteroid-belt"
	exit
fi

echo "" 
echo "Install complete - you may (optionally) remove the download folder"
